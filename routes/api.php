<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/articles',[Controllers\ArticleController::class,'getAllArticles']);
/*Route::get('/articles/{id}',[Controllers\ArticleController::class,'getArticle']);*/
Route::get('/articles/{article}',[Controllers\ArticleController::class,'getArticle']);

Route::middleware('auth:api')->group(function (){
    Route::post('/create-articles',[Controllers\ArticleController::class,'createArticle']);
    Route::put('/articles/{article}',[Controllers\ArticleController::class,'updateArticle']);
    Route::delete('/articles/{article}',[Controllers\ArticleController::class,'deleteArticle']);
});

Route::post('getToken',[Controllers\UserController::class,'generateToken']);


Route::get('/create-user',function (){

    User::forceCreate([
        'name'=>'Jon Doe',
        'email'=>'jon@doe.com',
        'password'=>Hash::make('12345678')
    ]);

    User::forceCreate([
        'name'=>'Jen Doe',
        'email'=>'jen@doe.com',
        'password'=>Hash::make('12345678')
    ]);

});

Route::get('create-token',function (){

    $user = User::find(1);
    $user->api_token = Str::random(80);
    $user->save();

    $user = User::find(2);
    $user->api_token = Str::random(80);
    $user->save();

});
