<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function getAllArticles(){
        $articles = Article::paginate(10);
        return $articles;
    }

    /*public function getArticle($id){

        $article = Article::findOrFail($id);
        return $article;
    }*/


    /*-----Get article with implicit binding*/

    public function getArticle(Article $article){
        return $article;
    }


    public function createArticle(Request $request){

        $title = $request->title;
        $content = $request->description;
        $user = $request->user();

        $article = new Article();
        $article->title = $title;
        $article->description = $content;
        $article->user_id = $user->id;
        $article->save();
        return $article;
    }

    public function updateArticle(Request $request, Article $article){

        $user = $request->user();

        if($user->id != $article->user_id){
            return response()->json(["error"=>"You don't have to permission!!"],404);
        }else{
            $title = $request->title;
            $description = $request->description;
            $article->title = $title;
            $article->description = $description;
            $article->save();
            return $article;
        }
    }

    public function deleteArticle(Request $request, Article $article){
        $user = $request->user();
        if($user->id != $article->user_id){
            return response()->json(["error"=>"You don't have to permission to delete!"]);
        }else{
            $article->delete();
            return response()->json(["success"=>"Article deleted successfully"],200);
        }
    }
}
